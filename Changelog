gems - Changelog

2009/12/06 - Version 1.1.1
	* main.c small fix: open() needs mode if O_CREAT in main.c (LP: #492996)
	* client/Makefile and server/Makefile: -s option argument of install
	  has been removed.
	* server/source/gems-server.c: Avoid sending an extra char to the
	  terminal when the terminal size is larger than suggested.

2004/09/19 - Version 1.1
	* Internationalization: Everything was translated into English, 
	  including code comments. Both server and client now support locales
	  via gettext. Spanish translation is included.
	* Server - Terminal size: gems-server now prints a warning message
	  if the terminal size is greater than 80x25.
	* Server - Lockfile: gems-server now uses a lock file to prevent
	  nested execution.
	* Client - New option: '-i' to ignore server terminal size.
	* Bugfixes: gems-server freezed if a connection was attempted
	  using telnet. This is now fixed. Several other minor bugs were
	  also fixed.

2004/03/05 - Version 1.0
	* Code: The main code was rewritten, for both client and server, so 
	  that they work more efficiently now, and also the server stores
	  some important information about the connected clients (e.g.: their
	  IP address).
	* Protocol: server and client now use our own communication protocol.
	  As a consequence, there is no compatibility with previous versions.
	  Bad luck :P
	* Terminal size: In order to solve the problems caused by the different
	  sizes (rows and columns) of server and client terminals, the
	  client will now close the connection if its terminal size is smaller
	  than the server's. Therefore, the server should be run in a terminal
	  not larger than 80x25, to be sure that clients will have no problems.
	* Server - script: Now the server runs "script -fq" automatically. This
	  allows simply running "gems-server" and getting a shell session 
	  opened to be transmitted to all the clients. This behavior can be
	  avoided with the parameter '-noscript', that will make the server
	  take the data to be transmitted directly from standard input, as it
	  used to be before. This behavior is not permanent; in the future
	  gems-server is planned to emulate script's functionality, without
	  actually executing it.
	* Server - logging: The option '-log' allows log messages to be written
	  either to an arbitrary file, stderr, or to the syslog daemon.
	* Server - Connections: The maximum number of allowed connections can
	  be specified with the option '-maxconn'. The option '-wait M' makes
	  the server wait for the connection of M clients before starting the
	  data transmission.
	* Client - Keys: Echoing is now disabled in the terminal, which means
	  that anything typed in the keyboard is not shown on screen.
	  Furthermore some features were assigned to certain keys; for
	  instance 'q' = quit. If a key with no assigned function is typed,
	  a warning beep is emmited.
	* Client - beep filtering: The 'a' key allows to enable/disable "beep
	  filtering" at run-time. When this feature is active, the client
	  ignores any alarm character ('\a') sent by the server so that it does
	  not play. This is especially useful when many clients are running in
	  the same physical room, to avoid the annoying simultaneous beeps.
	* Help: In both client and server, the option '-h' shows a brief help
	  message. Also, the option '-v' shows the program version. 
	* Manuals: The manual pages gems-server(1) and gems-client(1) were
	  written.

2003/12/29 - Version 0.2.0
	* Bugfix: "address already in use". Now the server can be run
	  immediately after being closed, without having to wait some minutes. 

2003/10/10 - Version 0.1.0
	First functional version. The server takes the data to transmit
	directly from the standard input, therefore, in order to start a
	script-like shell session, it is necessary to run something like:
	$ script -fq >(gems-server)


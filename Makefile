SUBDIRS = common client server doc po

# Destination root directory for install:
DESTDIR = /
# prefix: install architecture independent files in PREFIX:
PREFIX = usr/local
# exec-prefix: install architecture dependent files in EPREFIX:
EPREFIX = $(PREFIX)
# Path where to Install binaries:
BINPATH = $(DESTDIR)/$(EPREFIX)/bin
# Path where to install man pages:
MANPATH = $(DESTDIR)/$(PREFIX)/man
# Path where to install locale files:
LOCALEPATH = $(DESTDIR)/$(PREFIX)/share/locale

CC = gcc
CFLAGS = -O2
CDEFINES = -DLOCALEDIR=\"$(LOCALEPATH)\"
export CC CFLAGS CDEFINES BINPATH MANPATH LOCALEPATH

.PHONY: all $(SUBDIRS) $(SUBDIRS:%=%-clean) $(SUBDIRS:%=%-remove) \
	$(SUBDIRS:%=%-install) $(SUBDIRS:%=%-uninstall) \
	clean remove install uninstall

all: $(SUBDIRS)

$(SUBDIRS): %:
	$(MAKE) -C $*

$(SUBDIRS:%=%-clean): %-clean:
	$(MAKE) -C $* clean

$(SUBDIRS:%=%-remove): %-remove:
	$(MAKE) -C $* remove

$(SUBDIRS:%=%-install): %-install:
	$(MAKE) -C $* install

$(SUBDIRS:%=%-uninstall): %-uninstall:
	$(MAKE) -C $* uninstall

binary: $(SUBDIRS:%=%-binary)

clean: $(SUBDIRS:%=%-clean)

remove: $(SUBDIRS:%=%-remove)

install: $(SUBDIRS:%=%-install)

uninstall: $(SUBDIRS:%=%-uninstall)

debian-package: dist/debian
	ln -sf dist/debian debian
	fakeroot debian/rules binary
	rm debian


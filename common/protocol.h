/* ============================================================================
 * << File: protocol.h >>
 * -------------------------
 *  Authors: Emiliano Castagnari (aka Torian) <ecastag@fi.uba.ar>
 *           Diego Essaya <dessaya@fi.uba.ar>
 *     Date: 31/12/03
 * ============================================================================
 * ============================================================================
 *
 * ChangeLog: View Changelog
 *
 * ============================================================================
 * Copyright (C) 2003, 2004 Diego Essaya, Emiliano Castagnari
 * 
 * This file is part of gems.
 * 
 * gems is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * gems is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Library General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.
 * ============================================================================
 */

#ifndef _PROTOCOL_H_
#define _PROTOCOL_H_

#include "common.h"

/* Socket input/output buffer size: */
#define BUFSIZE         1024	

/* TCP ports: */
#define DEFAULT_TCP_PORT	6666
#define PORT_MIN        	1024
#define PORT_MAX        	65535

/* Port range verification
 * Ports lower than PORT_MIN are reserved by IANA */
#define port_range(p)        ((p >= PORT_MIN) && (p <= PORT_MAX))

/* Message field sizes, in bytes: */
#define MSG_TYPE_SIZE		1
#define MSG_DATA_LEN_SIZE	sizeof(int)   /* should be 4 */
#define MSG_MAX_DATA_SIZE	1024
/* Message header total size: */
#define MSG_HEADER_SIZE		((MSG_TYPE_SIZE) + (MSG_DATA_LEN_SIZE))

/* Types of messages, and their respective codes: */
typedef char gems_msg_type_t;
#define GEMS_DATA 	((gems_msg_type_t)0x00)
#define GEMS_WINSIZE	((gems_msg_type_t)0x01)
#define GEMS_VERSION	((gems_msg_type_t)0x02)
#define GEMS_ACK	((gems_msg_type_t)0x03)
#define GEMS_DISCONNECT	((gems_msg_type_t)0x04)

/* The size of message data is stored in an int (4 bytes): */
typedef unsigned int gems_msg_len_t;

/* Message structure: */
typedef struct
{
	gems_msg_type_t type;
	gems_msg_len_t len;
	char *data;
}
t_gems_msg;

/* I/O buffers, declared here to be used in other modules: */
extern char gems_msg_buffer[];
extern char *gems_header_buffer;
extern char *gems_data_buffer;

fbool_t get_stream(int fd, char *buffer, int bytes);
fbool_t get_msg(int fd, t_gems_msg * msg);
fbool_t send_msg(int fd, t_gems_msg * msg);
fbool_t send_version(int fd);
fbool_t get_peer_version(int fd, char **version);
fbool_t send_ack(int fd);
fbool_t get_ack(int fd);
fbool_t send_disconnect(int fd);
fbool_t send_winsize(int fd);
fbool_t compare_winsize(int remote_cols, int remote_rows, char *appname);
fbool_t get_cols_rows(const t_gems_msg * msg, int *cols, int *rows);
fbool_t get_winsize(int fd, int *cols, int *rows);

#endif

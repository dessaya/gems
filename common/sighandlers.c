/* ============================================================================
 * << File: sighandlers.c >>
 * -------------------------
 *  Authors: Emiliano Castagnari (aka Torian) <ecastag@fi.uba.ar>
 *           Diego Essaya <dessaya@fi.uba.ar>
 *     Date: 16/02/04
 *
 *  Description:
 *    Signal handling functions.
 * ============================================================================
 * ============================================================================
 *
 * ChangeLog: View Changelog
 *
 * ============================================================================
 * Copyright (C) 2003, 2004 Diego Essaya, Emiliano Castagnari
 * 
 * This file is part of gems.
 * 
 * gems is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * gems is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Library General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.
 * ============================================================================
 */

#include <signal.h>				/* signal() */

#include "sighandlers.h"

/* Global variables which notify the arrival of signals. These are the only
 * variables modified by the signal handlers: */
volatile int winsize_changed = 0;
volatile int sigint_raised = 0;
volatile int sigchld_raised = 0;


void install_sighandlers()
{
	signal(SIGINT, sigint_handler);
	signal(SIGWINCH, sigwinch_handler);
	signal(SIGCHLD, sigchld_handler);
}


void sigint_handler(int n)
{
	sigint_raised = 1;
	signal(n, sigint_handler);
}


void sigwinch_handler(int n)
{
	winsize_changed = 1;
	signal(n, sigwinch_handler);
}


void sigchld_handler(int n)
{
	sigchld_raised++;
	signal(n, sigchld_handler);
}

/* vim: set ts=4 sw=4: */


/* ============================================================================
 * << File: version.h >>
 * -------------------------
 *  Authors: Emiliano Castagnari (aka Torian) <ecastag@fi.uba.ar>
 *     Date: 23/01/04
 *  
 *  Description:
 *    Project name and version related declarations.
 * ============================================================================
 * ============================================================================
 *
 * ChangeLog: View Changelog
 *
 * ============================================================================
 * Copyright (C) 2003, 2004 Diego Essaya, Emiliano Castagnari
 * 
 * This file is part of gems.
 * 
 * gems is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * gems is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Library General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.
 * ============================================================================
 */

#ifndef _VERSION_H_
#define _VERSION_H_

#include <stdio.h>      /* printf() */

#include "common.h"

#define PROJECT_NAME    "gems"
#define PROJECT_VERSION "1.1"
#define PROTOCOL_VERSION "1.0" /* Communication protocol version */

/* Version message for both server and client: */
#define PRINT_VERSION(appname) \
{ \
	fprintf(stdout, _("%s %s\n" \
	"Copyright (C) 2003, 2004 Diego Essaya, Emiliano Castagnari. " \
	"Under GPL license.\n"), appname, PROJECT_VERSION); \
}

#endif

/* ============================================================================
 * << File: common.h >>
 * -------------------------
 *  Authors: Emiliano Castagnari (aka Torian) <ecastag@fi.uba.ar>
 *           Diego Essaya <dessaya@fi.uba.ar>
 *     Date: 29/12/03
 *
 *  Description:
 *    Declarations common to all project modules.
 * ============================================================================
 * ============================================================================
 *
 * ChangeLog: View Changelog
 *
 * ============================================================================
 * Copyright (C) 2003, 2004 Diego Essaya, Emiliano Castagnari
 * 
 * This file is part of gems.
 * 
 * gems is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * gems is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Library General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.
 * ============================================================================
 */

#ifndef _COMMON_H_
#define _COMMON_H_

/* gettext() support: */
#include <libintl.h>
#define _(String) gettext(String)
#define gettext_noop(String) (String)
#define N_(String) gettext_noop(String)

#include "version.h"

/* Enable debugging output? */
/*#define DEBUG
#define DEBUG_VERBOSE */

/* Not much to say about these: */
#define MAX(a, b)       ((a) >= (b) ? (a) : (b))
#define MIN(a, b)       ((a) <= (b) ? (a) : (b))

/* Tristate logic - for functions */
typedef enum { FAIL = -1, FALSE, TRUE } fbool_t;

/* OS return values: */
typedef enum { EXIT_OK = 0, EXIT_FAIL, CONTINUE } status_t;

#endif

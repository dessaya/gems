/* ============================================================================
 * << File: sock_info.h >>
 * -------------------------
 *  Authors: Emiliano Castagnari (aka Torian) <ecastag@fi.uba.ar>
 *           Diego Essaya <dessaya@fi.uba.ar>
 *     Date: 03/01/04
 *
 *  Description:
 *    Data types related to the connection information (IP, port, etc)
 *    and the client lists.
 * ============================================================================
 * ============================================================================
 *
 * ChangeLog: View Changelog
 *
 * ============================================================================
 * Copyright (C) 2003, 2004 Diego Essaya, Emiliano Castagnari
 * 
 * This file is part of gems.
 * 
 * gems is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * gems is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Library General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.
 * ============================================================================
 */

#ifndef _SOCK_INFO_H_
#define _SOCK_INFO_H_

/* IP address information: */
typedef struct t_ip
{
	unsigned long s_addr;		/* IP address in binary notation */
	char *ip_str;				/* IP address string */
}
t_ip;

/* TCP port information: */
typedef struct t_port
{
	unsigned short port;		/* Port number */
	char *port_str;				/* Port string */
}
t_port;

/* Client information: */
typedef struct t_clsock
{
	int socket;					/* Connection socket */
	t_ip ip;					/* IP address */
	struct t_clsock *next;		/* Double-linked list... you guessed it */
	struct t_clsock *prev;
}
t_clsock;

/* Connection-waiting client information: */
typedef struct t_waiting_client
{
	int socket;							/* Connection socket */
	struct sockaddr_in addr_data;		/* Address information */
	pid_t pid;			/* PID of the proccess that accepts the connection */
	struct t_waiting_client *next;		/* Another list... */
	struct t_waiting_client *prev;
}
t_waiting_client;

#endif

/* vim: set ts=4 sw=4: */

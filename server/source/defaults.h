/* ============================================================================
 * << File: defaults.h >>
 * ----------------------
 *  Authors: Emiliano Castagnari (aka Torian) <ecastag@fi.uba.ar>
 *           Diego Essaya <dessaya@fi.uba.ar>
 *     Date: 29/12/03
 *
 *  Description:
 *    Declarations of the default options for the server.
 * ============================================================================
 * ============================================================================
 *
 * ChangeLog: View Changelog
 *
 * ============================================================================
 * Copyright (C) 2003, 2004 Diego Essaya, Emiliano Castagnari
 * 
 * This file is part of gems.
 * 
 * gems is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * gems is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Library General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.
 * ============================================================================
 */
#ifndef _DEFAULTS_H_
#define _DEFAULTS_H_

#include <netinet/in.h> /* INADDR_ANY */

#define SERVER_DEF_IP   INADDR_ANY	/* Listen all the IPs */

/* Destination of logs. Possible values:
 *   NULL       -> don't log
 *   "filename" -> append log to "filename"
 *   "stderr"   -> log to stderr
 *   "syslog"   -> log through syslogd   */
#define SERVER_DEF_FLOG   NULL

#define SERVER_DEF_SCRIPT 1		/* Call script by default */
#define SERVER_DEF_SCRIPT_BIN	"/usr/bin/script"
#define SCRIPT_OPTIONS  "-fq"		/* See man script */

/* Lock filename prefix (followed by TCP port) */
#define LOCK_PREFIX	"/var/lock/gems-server."

/* Suggested maximum terminal size */
#define SUGGESTED_MAX_COLS	80
#define SUGGESTED_MAX_ROWS	25

/* Maximum simmultaneous client connections: */
#define SERVER_DEF_MAX_CONN	25

/* Number of client connections to wait before starting the transmission: */
#define SERVER_DEF_WAIT		0

/* FIXME: This info should be obtained from /etc/protocols; see 
 * getprotoent(3) */
#define PROTO_TCP       6		/* TCP - No modificar */
#define PROTO_UDP       17		/* UDP - No modificar */

#endif

/* ============================================================================
 * << File: log.h >>
 * -----------------
 *  Authors: Emiliano Castagnari (aka Torian) <ecastag@fi.uba.ar>
 *           Diego Essaya <dessaya@fi.uba.ar>
 *     Date: 29/12/03
 * ============================================================================
 * ============================================================================
 *
 * ChangeLog: View Changelog
 *
 * ============================================================================
 * Copyright (C) 2003, 2004 Diego Essaya, Emiliano Castagnari
 * 
 * This file is part of gems.
 * 
 * gems is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * gems is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Library General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.
 * ============================================================================
 */
#ifndef _LOG_H_
#define _LOG_H_

typedef enum
{ 
	ERR_NOT_OPT,
	ERR_OPT_WO_ARG,
	ERR_PORT,
	ERR_MAXCONN,
	ERR_WAIT,
	ERR_MEMORY,
	ERR_LOCK,
	ERR_INTERNAL,
	LOG_SOCK_C,
	LOG_NEW_C,
	LOG_CLOSE_C,
	LOG_CLOSING_ALL,
	LOG_MAXCONN,
	LOG_SERVER_VERSION,
	LOG_CLIENT_VERSION,
	LOG_WINSIZE,
	LOG_RUNSCRIPT,
	LOG_ENDSCRIPT,
	DEBUG_ADD_C,
	DEBUG_DEL_C,
	DEBUG_SEND_C
}
log_t;

/* Log message: */
typedef struct t_log
{
	log_t log_id;	/* ID number */
	int priority;	/* Type of message, same as syslog. Possible values:
					 * LOG_DEBUG, LOG_INFO, LOG_NOTICE, LOG_WARNING, LOG_ERR */
	char *log_str;	/* Message string */
}
t_log;

/* Log destinations: */
typedef enum 
{ G_LOG_NOLOG = 0, G_LOG_SYSLOG, G_LOG_FILE, G_LOG_STDERR }
log_opt_t;

/************************************************/

void g_set_logdest(log_opt_t _logdest, ...);
void g_log(short int, ...);
char *g_time(void);

#endif

/* vim: set ts=4 sw=4: */
